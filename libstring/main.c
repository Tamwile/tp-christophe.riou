#include <stdio.h>
#include <stddef.h>

size_t my_strlen(const char *s);
char *my_strchr(const char *s, int c);
char *my_strcpy(char *dest, const char *src);


int main()
{
  int taille = my_strlen("hello");
  printf("taille hello : %d\n", taille);

  return 0;
}


